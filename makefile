all: generate solve transform

generate: generate.o sudoku.o
	g++ -o generate generate.o sudoku.o

generate.o: generate.cpp
	g++ -c generate.cpp

solve: solve.o sudoku.o
	g++ -o solve solve.o sudoku.o

solve.o: solve.cpp
	g++ -c solve.cpp

transform: transform.o sudoku.o
	g++ -o transform transform.o sudoku.o

transform.o: transform.cpp
	g++ -c transform.cpp

sudoku.o: sudoku.cpp
	g++ -c sudoku.cpp

clean:
	rm generate solve transform *.o

