#include <iostream>
#include "sudoku.h"
using namespace std;


int main(){
	int su[9][9];
	for(int i=0;i<9;i++){
		for(int j=0;j<9;j++)
			cin>>su[i][j];
	}
    
    Sudoku S(su);
	S.pSudoku();
	while (1){
		int num;
		cin>>num;
		if(num == 1){
			int x;
			int y;
			cin>>x>>y;
			S.swapNum(x,y);
		}
        else if(num == 2){
			int x;
			int y;
			cin>>x>>y;
			S.swapRow(x*3,y*3);
			S.swapRow(x*3+1,y*3+1);
			S.swapRow(x*3+2,y*3+2);
		}
        else if(num == 3){
			int x;
			int y;
			cin>>x>>y;
			S.swapCol(x*3,y*3);
			S.swapCol(x*3+1,y*3+1);
			S.swapCol(x*3+2,y*3+2);
		}
        else if(num == 4){
			int x;
			cin>>x;
			S.rotate(x);
		}
        else if(num == 5){
			int x;
			cin>>x;
			S.flip(x);
		}
        else if(num == 0){
			break;
		}
	}
		S.pSudoku();
	return 0;
}

