#ifndef SUDOKU_H
#define SUDOKU_H

/***************************************************
 * Finish your .cpp according to this header file. *
 * You can modify this file if needed.             *
 ***************************************************/
#include <iostream>
using namespace std;

class Sudoku
{
public:
    Sudoku();
    Sudoku(int s[9][9]);
    void set_s(int s[9][9]);

    // generate
    void generate();

    // transform
    void swapNum(int x, int y);
    void swapRow(int x, int y);
    void swapCol(int x, int y);
    void rotate(int x);
    void flip(int x);
    void empty(int x,int y);
    void pSudoku();
    void pUnique();
    // solve
    void solve(int x,int y);
    /*int check;

    int checkRow();
    int CheckCol();
    int CheckCell();*/

    void next(int i,int j);
    bool can_fill(int x,int y,int k);
    bool can_Row(int x,int k);
    bool can_Col(int y,int k);
    bool can_Cell(int x,int y,int k);

private:
    int  sudoku[9][9]={
                      {5,3,4,6,7,8,9,1,2},
                      {6,7,2,1,9,5,3,4,8},
                      {1,9,8,3,4,2,5,6,7},
                      {8,5,9,7,6,1,4,2,3},
                      {4,2,6,8,5,3,7,9,1},
                      {7,1,3,9,2,4,8,5,6},
                      {9,6,1,5,3,7,2,8,4},
                      {2,8,7,4,1,9,6,3,5},
                      {3,4,5,2,8,6,1,7,9}
    };

    //vector < vector <int> steps;
    int qty_s=0;

    int unique[9][9];
};




#endif // SUDOKU_H
