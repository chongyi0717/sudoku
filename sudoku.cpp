#include<iostream>
#include"sudoku.h"
#include<ctime>
#include<cstdlib>
#include <cstdio>
using namespace std;

Sudoku::Sudoku(){
    srand(time(NULL));
//    this->pSudoku();
}

Sudoku::Sudoku(int s[9][9]){
    for(int i=0;i<9;++i){
        for(int j=0;j<9;++j)
            sudoku[i][j]=s[i][j];
    }
}

void Sudoku::set_s(int s[9][9]){
    for(int i=0;i<9;++i){
        for(int j=0;j<9;++j)
            sudoku[i][j]=s[i][j];
    }
}

void Sudoku::generate(){
    //this->pSudoku();

    int times=rand()%1000+1000;
    for(int i=0;i<times;++i){
        //int t=rand()%3*3;
        int x=rand()%9+1;
        int y=rand()%9+1;
       this->swapNum(x,y);
    }

     times = rand()%75;
    for(int i=0;i<times;++i){
        int t = rand()%3*3;
        int x = rand()%3+t;
        int y = rand()%3+t;
        this->swapRow(x,y);
    }

      times = rand()%60;
      for(int i=0;i<times;++i){
          int t = rand()%3*3;
          int x = rand()%3+t;
          int y = rand()%3+t;
          this->swapCol(x,y);
      }

        this->rotate(rand()%4);

        this->flip(rand()%2);

        for(int i = 0 ; i< rand()%60; ++i){
            int x=rand()%9;
            int y=rand()%9;
            //this->empty(x,y);
        }

        this->pSudoku();


}

void Sudoku::swapNum(int x,int y){
    for (int i =0 ; i< 9; ++i){
        for(int j =0;j<9 ;++j){
            if (this->sudoku[i][j]==x)
                this->sudoku[i][j]=y;
            else if(this->sudoku[i][j]==y)
                this->sudoku[i][j]=x;
        }
    }

}

void Sudoku::swapRow(int x,int y){
    for (int i =0;i<9;++i){
        int t=sudoku[x][i];
        this->sudoku [x][i]=this->sudoku[y][i];
        this->sudoku[y][i]=t;
    }
}

void Sudoku::swapCol(int x,int y){
    for(int i=0;i<9;i++){
        int t=sudoku[i][x];
        this->sudoku[i][x]=this->sudoku[i][y];
        this->sudoku[i][y]=t;
    }
}

void Sudoku::rotate(int x){
    int copy[9][9];
    for (int i = 0 ; i<9 ; ++i){
        for (int j =0 ; j<9 ;++j)
        copy[i][j]=this->sudoku[i][j];
    }
    
    for (int i =0 ; i < x ; ++i){
        for ( int j = 8 ; j>= 0 ; --j){
            for (int k =0 ; k <9 ; ++k){
               this-> sudoku[k][j] = copy[8-j][k];
            }
        }
    }
}

void Sudoku ::flip(int x){
    if (x==0){
        for(int i=0;i<4;++i){
            this->swapRow(i, 8-i);
        }
    }
     else if (x==1){
         for (int i = 0 ; i <4 ; ++i ){
            this-> swapCol(i,8-i);
         }
     }  
  }

void Sudoku ::empty (int x,int y){
    this->sudoku[x][y]=0;
}

void Sudoku::pSudoku(){
    for (int i =0;i<9;++i){
        for(int j =0; j<9;++j){
        cout<<sudoku[i][j];
        if(j!=8)
            cout<<" ";
        else
            cout<<endl;
        }
    }
}

void Sudoku::pUnique(){
    if(qty_s==1){
        for(int i=0;i<9;++i){
            for(int j=0;j<9;++j){
                cout<<unique[i][j];
                if(j!=8)
                    cout<<"  ";
                else
                    cout<<endl;
            }
        }
    }else if(qty_s==0)
        cout<<"0"<<endl;
}

void Sudoku::next(int i,int j){
    j==8?solve(i+1,0):solve(i,j+1);
}

void Sudoku::solve(int x,int y){
    if(x==9){
        ++qty_s;
        if(qty_s==2){
            cout<<"2"<<endl;
            exit(0);
        }
        for(int i=0;i<9;++i){
            for(int j =0 ;j<9;++j){
                unique[i][j]=sudoku[i][j];
            }
        }
        return;
    }
    if (sudoku[x][y]!=0){
        next(x,y);
        return;
    }
    else if(sudoku[x][y]==0){
        for(int k=1;k<=9;++k){
            if(can_fill(x,y,k)){
                sudoku[x][y]=k;
                next(x,y);
                sudoku[x][y]=0;
            }
        }
    }
}

bool Sudoku::can_fill(int x,int y,int k){
    return can_Row(x,k) && can_Col(y,k) && can_Cell(x,y,k);
}

bool Sudoku::can_Row(int x,int k){
    for(int i=0;i<9;++i){
        if(sudoku[x][i]==k)
            return false;
    }
    return true;
}

bool Sudoku::can_Col(int y,int k){
    for(int i=0;i<9;++i){
        if (sudoku[i][y]==k)
            return false;
    }
    return true;
}

bool Sudoku::can_Cell(int x,int y,int k){
    int cell_x=x/3*3;
    int cell_y=y/3*3;

    for(int i=cell_x;i<cell_x+3; ++i){
        for (int j=cell_y; j <cell_y+3;++j){
        if(sudoku[i][j]==k)
         return false;
    }
}
return true;
}



